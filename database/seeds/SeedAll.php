<?php

use Illuminate\Database\Seeder;
use App\Barang;
use App\Supplier;
use App\Karyawan;
class SeedAll extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Barang::create([
            'nama_atk'=>'Kertas A4',
            'jumlah'=>'12',
            'satuan'=>'Rim',
            'active'=>'1'
          ]);

        Barang::create([
            'nama_atk'=>'Kertas F4',
            'jumlah'=>'15',
            'satuan'=>'Rim',
            'active'=>'1'
          ]);
        Barang::create([
            'nama_atk'=>'Paper Clip',
            'jumlah'=>'12',
            'satuan'=>'Box',
            'active'=>'1'
          ]);

        Supplier::create([
            'nama' => 'Supplier 1',
            'alamat' => 'Tes',
            'no_telp' => '123123'
        ]);
        
        Supplier::create([
            'nama' => 'Supplier 2',
            'alamat' => 'Jl. Cinta',
            'no_telp' => '5454545'
        ]);

        Karyawan::create([
            'nm_karyawan' => 'Supardi',
            'alamat' => 'Jl. Kenangan',
            'no_telp' => '5454545',
            'jk' => 'L'
        ]);

        Karyawan::create([
            'nm_karyawan' => 'Ely',
            'alamat' => 'Jl. Lay',
            'no_telp' => '1235123',
            'jk' => 'P'
        ]);

    }
}
