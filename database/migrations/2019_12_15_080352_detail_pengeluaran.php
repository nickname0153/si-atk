<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetailPengeluaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pengeluaran', function (Blueprint $table) {
            $table->unsignedBigInteger('pengeluaran_id');
            $table->unsignedBigInteger('barang_id');
            $table->integer('jumlah', 25);
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->foreign('pengeluaran_id')->references('id')->on('pengeluaran')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('barang_id')->references('id')->on('barang')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pengeluaran');
    }
}
