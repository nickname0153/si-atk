<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pengeluaran;

class Karyawan extends Model
{
    protected $table = "karyawan";

    public function Pengeluaran()
    {
        return $this->belongsTo(Pengeluaran::class);
    }
}
