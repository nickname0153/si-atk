<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailPemasukan;
use App\DetailPengeluaran;

class Barang extends Model
{
    protected $table = "barang";

    public function Pemasukan()
    {
        return $this->belongsTo(DetailPemasukan::class);
    }

    public function Pengeluaran()
    {
        return $this->belongsTo(DetailPengeluaran::class);
    }
}
