<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailPengeluaran;
use App\Karyawan;

class Pengeluaran extends Model
{
    protected $table = "pengeluaran";

    public function karyawan()
    {
        return $this->hasMany(Karyawan::class);
    }

    public function DetailPengeluaran()
    {
        return $this->belongTo(DetailPengeluaran::class);
    }
}
