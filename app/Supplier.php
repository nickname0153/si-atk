<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pemasukan;

class Supplier extends Model
{
    protected $table = "supplier";

    public function Pemasukan()
    {
        return $this->belongsTo(Pemasukan::class);
    }
}
