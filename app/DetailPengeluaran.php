<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Barang;
use App\Pengeluaran;

class DetailPengeluaran extends Model
{
    protected $table = "detail_pengeluaran";

    public function Pengeluaran()
    {
        return $this->hasMany(Pengeluaran::class);
    }
    
    public function Barang()
    {
        return $this->hasMany(Barang::class);
    }
}
