<?php

namespace App\Http\Controllers;

use App\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = Barang::all();
        // exit();
        return view('barang.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang = New Barang();
        $barang->nama_atk = $request->nama_atk;
        $barang->jumlah = $request->jumlah;
        $barang->satuan = $request->satuan;
        if($request->gambar)
        {
            $lokasigambar = $request->gambar->store('public/atk');
            $barang->gambar = $lokasigambar;
        }
        $barang->save();
        return redirect()->route('barang.index')->with('alert-success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show($barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit($barang)
    {
        $barangs = Barang::find($barang);
        return view('barang.edit', compact('barangs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $barang)
    {
        $atk = Barang::find($barang);
        $atk->nama_atk = $request->nama_atk;
        $atk->jumlah = $request->jumlah;
        $atk->satuan = $request->satuan;
        if($request->gambar)
        {
            $lokasigambar = $request->gambar->store('public/atk');
            $atk->gambar = $lokasigambar;
        }
        $atk->update();
        return redirect()->route('barang.index')->with('alert-info', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($barang)
    {
        $data = Barang::find($barang);
        $data->delete();
        return redirect()->route('barang.index')->with('alert-danger', 'Data Berhasil Dihapus');

    }
}
