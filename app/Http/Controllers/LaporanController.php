<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use PDF;

class LaporanController extends Controller
{
    public function barang()
    {
        $data = Barang::all();
        return view('laporan.barang', compact('data'));
    }
    public function cetakbarang()
    {
    	$data = Barang::all();
 
    	$pdf = PDF::loadview('laporan.barang_pdf',['data'=>$data]);
    	return $pdf->download('laporan-barang.pdf');
    }
}
