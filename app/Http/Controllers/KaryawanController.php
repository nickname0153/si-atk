<?php

namespace App\Http\Controllers;

use App\Karyawan;
use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $karyawans = Karyawan::all();
        return view('karyawan.index', compact('karyawans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $karyawans = New Karyawan();
        $karyawans->nm_karyawan = $request->nm_karyawan;
        $karyawans->alamat = $request->alamat;
        $karyawans->no_telp = $request->no_telp;
        $karyawans->jk = $request->jk;
        $karyawans->save();
        return redirect()->route('karyawan.index')->with('alert-success', 'Data Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function show($karyawan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function edit($karyawan)
    {
        $data = Karyawan::find($karyawan);
        return view('karyawan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $karyawan)
    {
        $karyawans = Karyawan::find($karyawan);
        $karyawans->nm_karyawan = $request->nm_karyawan;
        $karyawans->alamat = $request->alamat;
        $karyawans->no_telp = $request->no_telp;
        $karyawans->jk = $request->jk;
        $karyawans->update();
        return redirect()->route('karyawan.index')->with('alert-success', 'Data Berhasil Ditambah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function destroy($karyawan)
    {
        $data = Karyawan::find($karyawan);
        $data->delete();
        return redirect()->route('karyawan.index')->with('alert-danger', 'Data Berhasil Dihapus');
    }
}
