<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Barang;
use App\Pemasukan;

class DetailPemasukan extends Model
{
    protected $table = "detail_pemasukan";

    public function Pemasukan()
    {
        return $this->hasMany(Pemasukan::class);
    }
    
    public function Barang()
    {
        return $this->hasMany(Barang::class);
    }
}
