@extends('layout.main')
@section('title', 'Pemakaian - SI Pemakaian ATK')
@section('content')
<div class="row">
    <div class="col-sm-6">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Data Pemakai</h3>
            </div>
            <form role="form">
                <div class="card-body">
                    <div class="form-group">
                        <label>Karyawan</label>
                        {!! Form::select('karyawan', $karyawan, null, ['class' => 'form-control select2', 'placeholder'
                        => 'Pilih Karyawan']) !!}
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        {!! Form::textarea('keterangan', null, ['class'=>'form-control', 'rows' => '3']) !!}
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Pakai ATK</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Barang ATK</label>
                    {!! Form::select('nama_atk', $nama_atk, null, ['class' => 'form-control select2', 'placeholder'
                    => 'Pilih Barang ATK']) !!}
                </div>
                <div class="form-group">
                    <label>Jumlah Pakai</label>
                    {!! Form::number('jumlah', null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="card-footer">
                <a href="" class="btn btn-primary">Simpan</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-warning">
            <div class="card-header">
                <h3 class="card-title">Data Barang</h3>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                   <thead>
                       <tr>
                           <td>Nama ATK</td>
                           <td>Satuan</td>
                           <td>Jumlah</td>
                       </tr>
                   </thead>
                   <tbody>
                       <tr>
                           <td>Kertas A4</td>
                           <td>Rim</td>
                           <td>2</td>
                       </tr>
                       <tr>
                           <td>Paper Clip</td>
                           <td>Box</td>
                           <td>4</td>
                       </tr>
                   </tbody>
                   <tfoot>
                       <tr>
                           <td colspan="3"><a href="" class="btn btn-primary">Simpan</a></td>
                       </tr>
                   </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('costumjs')
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });
    });

</script>
@endsection
