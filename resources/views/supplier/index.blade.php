@extends('layout.main')
@section('title', 'Data Karyawan - SI Pemakaian ATK')
@section('content-header', 'Data Supplier')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                {!! Form::button('<i class="fa fa-plus"></i>', ['class' => 'btn btn-primary', 'data-toggle' => 'modal',
                'data-target' => '#modal-add']) !!}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>Nama Supplier</th>
                            <th>Alamat</th>
                            <th>No Telpon</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $i)
                        <tr class="text-center">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$i->nama}}</td>
                            <td>{{$i->alamat}}</td>
                            <td>{{$i->no_telp}}</td>
                            <td>
                                <a class="btn btn-success btn-md" data-toggle="modal" data-target="#modal-edit-{{$i->id}}"><i class="fa fa-pencil-alt"></i></a>
                                <div class="modal fade" id="modal-edit-{{$i->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Ubah Supplier</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            {!! Form::open(['route'=>['supplier.update', $i->id],'method'=>'patch','files'=>true])
                                            !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('nama', 'Masukan Nama Supplier', ['class'=>'control-label']) !!}
                                                    {!! Form::text('nama', $i->nama, ['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('alamat', 'Masukan Alamat', ['class'=>'control-label']) !!}
                                                    {!! Form::textarea('alamat', $i->alamat, ['class'=>'form-control','placeholder'=>'Masukan Alamat', 'rows' => '3'])!!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('no_telp', 'Masukan No. Telp', ['class'=>'control-label']) !!}
                                                    {!! Form::text('no_telp', $i->no_telp, ['class'=>'form-control','placeholder'=>'Masukan Nomor Telpon'])!!}
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Close</button>
                                                {{ Form::button('<i class="fa fa-pencil-alt"> Ubah Data</i>', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                {!! Form::open(['route'=>['supplier.destroy',$i->id],'method'=>'DELETE']) !!}
                                {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick'=>"return confirm('Yakin ingin mengapus data?')"] )  }}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Supplier</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>'supplier.store','method'=>'post','files'=>true]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nama', 'Masukan Nama Supplier', ['class'=>'control-label']) !!}
                    {!! Form::text('nama', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('alamat', 'Masukan Alamat', ['class'=>'control-label']) !!}
                    {!! Form::textarea('alamat', null, ['class'=>'form-control','placeholder'=>'Masukan Alamat', 'rows' => '3'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('no_telp', 'Masukan No. Telp', ['class'=>'control-label']) !!}
                    {!! Form::text('no_telp', null, ['class'=>'form-control','placeholder'=>'Masukan Nomor Telpon'])!!}
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::button('<i class="fa fa-plus"> Tambah Data</i>', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('costumjs')
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
@endsection
