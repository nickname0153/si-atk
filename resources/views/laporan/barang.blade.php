@extends('layout.main')
@section('title', 'Laporan Barang - SI Pemakaian ATK')
@section('content')
<div class="row">
    <div class="col-sm-4">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Data Filter</h3>
            </div>
            <form role="form">
                <div class="card-body">
                    <div class="form-group">
                        <label>Filter Tanggal Dari - Sampai</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                              </span>
                            </div>
                            <input type="text" class="form-control float-right" id="reservation">
                          </div>
                    </div>
                </div>
            </form>
            <div class="card-footer">
                <button class="btn btn-success">Filter Data</button>
                <a href="{{route('laporan.cetakbarang')}}" target="_blank" class="btn btn-primary">Cetak Data (PDF)</a>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="card card-default">
            <div class="card-header">
                <h3 class="card-title">Data Barang</h3>
            </div>
            <div class="card-body">
                <table id="example2" class="table table-bordered table-striped">
                   <thead>
                       <tr>
                           <td>Nama ATK</td>
                           <td>Satuan</td>
                           <td>Jumlah</td>
                       </tr>
                   </thead>
                   <tbody>
                       @foreach ($data as $barang)
                           <tr>
                                <td>{{$barang->nama_atk}}</td>
                                <td>{{$barang->satuan}}</td>
                                <td>{{$barang->jumlah}}</td>
                           </tr>
                       @endforeach
                   </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('costumjs')
<!-- date-range-picker -->
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script>
    $(function () {
        $('#reservation').daterangepicker()

        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
@endsection
