@extends('layout.main')
@section('title', 'Data Karyawan - SI Pemakaian ATK')
@section('content-header', 'Data Karyawan')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                {!! Form::button('<i class="fa fa-plus"></i>', ['class' => 'btn btn-primary', 'data-toggle' => 'modal',
                'data-target' => '#modal-add']) !!}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>Nama Karyawan</th>
                            <th>Alamat</th>
                            <th>No Telpon</th>
                            <th>Jenis Kelamin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($karyawans as $i)
                        <tr class="text-center">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$i->nm_karyawan}}</td>
                            <td>{{$i->alamat}}</td>
                            <td>{{$i->no_telp}}</td>
                            <td>@if($i->jk == 'L')
                                    Laki-laki
                                @else
                                    Perempuan
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-success btn-md" data-toggle="modal" data-target="#modal-edit-{{$i->id}}"><i class="fa fa-pencil-alt"></i></a>
                                <div class="modal fade" id="modal-edit-{{$i->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Ubah karyawan</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            {!! Form::open(['route'=>['karyawan.update', $i->id],'method'=>'patch','files'=>true])
                                            !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('nm_karyawan', 'Masukan Nama Karyawan', ['class'=>'control-label']) !!}
                                                    {!! Form::text('nm_karyawan', $i->nm_karyawan, ['class'=>'form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('alamat', 'Masukan Alamat', ['class'=>'control-label']) !!}
                                                    {!! Form::textarea('alamat', $i->alamat, ['class'=>'form-control','placeholder'=>'Masukan Alamat', 'rows' => '3'])!!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('no_telp', 'Masukan No. Telp', ['class'=>'control-label']) !!}
                                                    {!! Form::text('no_telp', $i->no_telp, ['class'=>'form-control','placeholder'=>'Masukan Nomor Telpon'])!!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('jk', 'Jenis Kelamin', ['class'=>'control-label']) !!}
                                                    {!! Form::select('jk', ['L' => 'Laki-Laki', 'P'=>'Perempuan'], $i->jk, ['class'=>'form-control','placeholder'=>'Pilih Jenis Kelamin'])!!}
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Close</button>
                                                {{ Form::button('<i class="fa fa-pencil-alt"> Ubah Data</i>', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                {!! Form::open(['route'=>['karyawan.destroy',$i->id],'method'=>'DELETE']) !!}
                                {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick'=>"return confirm('Yakin ingin mengapus data?')"] )  }}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>'karyawan.store','method'=>'post','files'=>true]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nm_karyawan', 'Masukan Nama Karyawan', ['class'=>'control-label']) !!}
                    {!! Form::text('nm_karyawan', null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('alamat', 'Masukan Alamat', ['class'=>'control-label']) !!}
                    {!! Form::textarea('alamat', null, ['class'=>'form-control','placeholder'=>'Masukan Alamat', 'rows' => '3'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('no_telp', 'Masukan No. Telp', ['class'=>'control-label']) !!}
                    {!! Form::text('no_telp', null, ['class'=>'form-control','placeholder'=>'Masukan Nomor Telpon'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('jk', 'Jenis Kelamin', ['class'=>'control-label']) !!}
                    {!! Form::select('jk', ['L' => 'Laki-Laki', 'P'=>'Perempuan'], null, ['class'=>'form-control','placeholder'=>'Pilih Jenis Kelamin'])!!}
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::button('<i class="fa fa-plus"> Tambah Data</i>', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('costumjs')
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
@endsection
