@extends('layout.main')
@section('title', 'Data Barang - SI Pemakaian ATK')
@section('content-header', 'Data Barang')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                {!! Form::button('<i class="fa fa-plus"></i>', ['class' => 'btn btn-primary', 'data-toggle' => 'modal',
                'data-target' => '#modal-add']) !!}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>Nama</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Gambar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($barangs as $i)
                        <tr class="text-center">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$i->nama_atk}}</td>
                            <td>{{$i->jumlah}}</td>
                            <td>{{$i->satuan}}</td>
                            <td><img src="{{Storage::url($i->gambar)}}" alt="" width="50px" height="50px"></td>
                            <td>
                                <a class="btn btn-success btn-md" data-toggle="modal" data-target="#modal-edit-{{$i->id}}"><i class="fa fa-pencil-alt"></i></a>
                                <div class="modal fade" id="modal-edit-{{$i->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Ubah Barang</h4>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            {!! Form::open(['route'=>['barang.update', $i->id],'method'=>'patch','files'=>true])
                                            !!}
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    {!! Form::label('nama_atk', 'Masukan Nama ATK', ['class'=>'control-label']) !!}
                                                    {!! Form::text('nama_atk', $i->nama_atk, ['class'=>'form-control','placeholder'=>'Masukan Nama ATK']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('jumlah', 'Masukan Jumlah',
                                                    ['class'=>'control-label']) !!}
                                                    {!! Form::number('jumlah', $i->jumlah,
                                                    ['class'=>'form-control','placeholder'=>'Masukan Jumlah'])!!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('satuan', 'Masukan Satuan',
                                                    ['class'=>'control-label']) !!}
                                                    {!! Form::text('satuan', $i->satuan,
                                                    ['class'=>'form-control','placeholder'=>'Masukan Satuan'])!!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('gambar', 'Masukan Foto',
                                                    ['class'=>'control-label']) !!}
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            {!! Form::file('gambar',
                                                            ['class'=>'custom-file-input','id'=>'exampleInputFile'])
                                                            !!}
                                                            {!! Form::label('gambar', 'Pilih Foto',
                                                            ['class'=>'custom-file-label']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Close</button>
                                                {{ Form::button('<i class="fa fa-pencil-alt"> Ubah Data</i>', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                {!! Form::open(['route'=>['barang.destroy',$i->id],'method'=>'DELETE']) !!}
                                {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-md', 'onclick'=>"return confirm('Yakin ingin mengapus data?')"] )  }}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Barang</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['route'=>'barang.store','method'=>'post','files'=>true]) !!}
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('nama_atk', 'Masukan Nama ATK', ['class'=>'control-label']) !!}
                    {!! Form::text('nama_atk', null, ['class'=>'form-control','placeholder'=>'Masukan Nama ATK']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('jumlah', 'Masukan Jumlah', ['class'=>'control-label']) !!}
                    {!! Form::number('jumlah', null, ['class'=>'form-control','placeholder'=>'Masukan Jumlah'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('satuan', 'Masukan Satuan', ['class'=>'control-label']) !!}
                    {!! Form::text('satuan', null, ['class'=>'form-control','placeholder'=>'Masukan Satuan'])!!}
                </div>
                <div class="form-group">
                    {!! Form::label('gambar', 'Masukan Foto', ['class'=>'control-label']) !!}
                    <div class="input-group">
                        <div class="custom-file">
                            {!! Form::file('gambar',
                            ['class'=>'custom-file-input','id'=>'exampleInputFile','required'])
                            !!}
                            {!! Form::label('gambar', 'Pilih Foto', ['class'=>'custom-file-label']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{ Form::button('<i class="fa fa-plus"> Tambah Data</i>', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('costumjs')
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
        });
    });

</script>
@endsection
