<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@login')->name('dashboard.login');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
Route::resource('/barang', 'BarangController');
Route::resource('/karyawan', 'KaryawanController');
Route::resource('/supplier', 'SupplierController');
Route::name('pemakaian.')->prefix('pemakaian')->group(function(){
    Route::get('/', 'PengeluaranController@index')->name('index');
});
Route::name('pemasukan.')->prefix('pemasukan')->group(function(){
    Route::get('/', 'PemasukanController@index')->name('index');
});
Route::name('laporan.')->prefix('laporan')->group(function(){
    Route::get('/barang', 'LaporanController@barang')->name('barang');
    Route::get('/cetakbarang', 'LaporanController@cetakbarang')->name('cetakbarang');
});
// Route::resource('/pemasukan')->prefix('pemasukan')->name('pemasukan')
